-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-09-2019 a las 21:15:38
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `amigos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `amigos`
--

CREATE TABLE `amigos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `foto` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `amigos`
--

INSERT INTO `amigos` (`id`, `nombre`, `foto`, `created_at`) VALUES
(1, 'Luis', 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/U09_Luis_Su%C3%A1rez_7540.jpg/1200px-U09_Luis_Su%C3%A1rez_7540.jpg', '2019-09-27 18:36:27'),
(2, 'Iván', 'https://www.rittnerbuam.com/wp-content/uploads/2018/12/Tauferer_Ivan_Ritten_Pustertal_29_11_2018_Pattis.jpg', '2019-09-27 18:36:27'),
(3, 'Ana', 'https://www.brandesautographs.com/media/catalog/product/cache/2/thumbnail/9df78eab33525d08d6e5fb8d27136e95/6/0/6012467.jpg', '2019-09-27 18:38:00'),
(4, 'Estefanía', 'http://4.bp.blogspot.com/-hiAnM7MQlDM/TeKmez8U2MI/AAAAAAAAF0o/G7m1ZVY36U0/s1600/estefania_kuester_picture_hd.jpg', '2019-09-27 18:38:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `amigos`
--
ALTER TABLE `amigos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre_unico` (`nombre`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `amigos`
--
ALTER TABLE `amigos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
