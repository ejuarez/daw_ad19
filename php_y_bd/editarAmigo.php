<?php
    
    require_once("modelo.php");
    //Inicio o recuperdo la sesión
    session_start();

    include("_header.html");

    if(isset($_GET["id"])) {
        $amigo = get_amigo($_GET["id"]);
        include("_editarAmigo.html");
    } else {
        echo tarjeta_amigos();
    }
    
    
    include("_footer.html");
?>