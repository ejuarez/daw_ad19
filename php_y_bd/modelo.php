<?php

/**
 * Crea una conexión con una base de datos en mysql
 **/
function connectDB() {
    
    //variable para configurar la conexión a la base de datos dependiendo del ambiente:
    //DEV: Ambiente de desarrollo
    //PROD: Ambiente de producción
    //TEST: Ambiente de pruebas
    $environment = "DEV";
    
    if ($environment == "DEV") {
         $bd = mysqli_connect("localhost","root","","amigos");
    } else if($environment == "PROD") {
         //TODO: Cambiar la configuración de acuerdo al ambiente de producción
         $bd = mysqli_connect("localhost","root","passwdadmin","amigos");
    }
    
    // Change character set to utf8
    mysqli_set_charset($bd,"utf8");
   
    return $bd;
}

function closeDB($bd) {
    
    mysqli_close($bd);
}

/**
 * Genera tarjetas de amigos
 * $id: id del amigo si sólo se quiere ver un amigo
 * return: un string con el html de las tarjetas de amigos
 **/
function tarjeta_amigos($id=0) {
    
    $db = connectDB();
    
    $resultado = '<div class="row">';
    
    $query = 'SELECT id, nombre, foto, created_at FROM amigos';
    
    if($id != 0) {
        $query .= " WHERE id=$id";
    }
    
    // Query execution; returns identifier of the result group
    $registros = $db->query($query);
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
        $resultado .= '
            <div class="col s12 m6 l4">
                <div class="card">
                    <div class="card-image">
                        <a href="verAmigos.php?id='.$fila["id"].'"><img src="'.$fila["foto"].'"></a>
                        <span class="card-title">'.$fila["nombre"].'</span>
                    </div>
                    <div class="card-content">
                        <p>Miembro desde '.$fila["created_at"].'<a href="editarAmigo.php?id='.$fila["id"].'"><i class="material-icons">edit</i></a></p>
                    </div>
                </div>
            </div>
        ';
    }
    
    $resultado .= "</div>";
    
    // it releases the associated results
    mysqli_free_result($registros);
    // Options: MYSQLI_NUM to use only numeric indexes
    // MYSQLI_ASSOC to use only name (string) indexes
    // MYSQLI_BOTH, to use both

    closeDB($db);
    
    return $resultado;
}

function nuevo_amigo($nombre, $foto) {
    
    $db = connectDB();
    
    // insert command specification 
    $query='INSERT INTO amigos (nombre,foto) VALUES (?,?) ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("No se pudo preparar la consulta para la bd: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ss", $nombre, $foto)) {
        die("Falló la vinculación de los parámetros: (" . $statement->errno . ") " . $statement->error); 
    }
    
    // Executing the statement
    if (!$statement->execute()) {
        die("Falló la ejecución de la consulta: (" . $statement->errno . ") " . $statement->error);
    } 

    closeDB($db);
}

function get_amigo($id) {
    $db = connectDB();
    
    $query = "SELECT id, nombre, foto FROM amigos WHERE id=$id";
    
    // Query execution; returns identifier of the result group
    $registros = $db->query($query);
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
        $amigo["nombre"] = $fila["nombre"];
        $amigo["foto"] = $fila["foto"];
        $amigo["id"] = $fila["id"];
    }

    // it releases the associated results
    mysqli_free_result($registros);

    closeDB($db);
    
    return $amigo;
}

function update_amigo($id, $nombre, $foto) {
    
    $db = connectDB();
    
    $amigo_anterior=get_amigo($id);
    
    if($foto == "") {
        $foto = $amigo_anterior["foto"];
    } else {
        $nombre_foto = $foto;
        $foto = "uploads/".$nombre_foto;
    }
    
    // insert command specification 
    $query='UPDATE amigos SET nombre=?, foto=? WHERE id=?';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("No se pudo preparar la consulta para la bd: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("sss", $nombre, $foto, $id)) {
        die("Falló la vinculación de los parámetros: (" . $statement->errno . ") " . $statement->error); 
    }
    
    // Executing the statement
    if (!$statement->execute()) {
        die("Falló la ejecución de la consulta: (" . $statement->errno . ") " . $statement->error);
    }

    closeDB($db);
}

?>